
jQuery(document).ready(function(){

/* calls function to display permalink for deep link item  */
dlPlink();

/*calls function to display permalink in uResolver*/
resolverPlink();



// add placeholder text to search box input
// Anneliese Dehner
// 2015
	var s_tab = $("#tab").val();

        // LCC
        if (s_tab == "lc_only") {
        	$(".EXLSearchFieldRibbonFormSearchFor input").attr("placeholder","Find books and more at Lewis & Clark");
    	} 

	// LCC + Summit
        if (s_tab == "lc_summit") { 
                 $(".EXLSearchFieldRibbonFormSearchFor input").attr("placeholder","Find books and more at NW academic libraries");
        } 
                        
        // LCC + Summit + Articles
        if (s_tab == "default_tab") {
                 $(".EXLSearchFieldRibbonFormSearchFor input").attr("placeholder","Find books, AV materials, and articles");
	}




// show sign-in bar if user is not signed in
// Anneliese Dehner
// 2015

	//show
	if ($('#exlidSignOut').hasClass('EXLHidden')) {
		var signInLink = $('#exlidSignIn a').attr('href');
          	var msg = "<a href='" + signInLink + "'>Sign in for enhanced features and complete results.</a>";

		if ($("body").hasClass("EXLFullView")) {
			$('#exlidSearchTileWrapper').append('<div id="exlidHeaderSystemFeedbackContent" class="EXLSystemFeedback"><strong>' + msg + '</strong></div>');
		} else {
			$('#exlidHeaderSystemFeedback').append('<div id="exlidHeaderSystemFeedbackContent" class="EXLSystemFeedback"><strong>' + msg + '</strong></div>');

		}
	}

	//hide
	if ($("#exlidSignIn").hasClass("EXLHidden")) {
          	$("#signInHomeBody strong").hide();
        }





// hide back to results link in services page and add advanced search link and browse search link to search widgets
// Anneliese Dehner
// 2015

	var vid = GetURLParameter("vid");
	if (vid == "lcc_services_page"){

		function getParameterByName(name) {
    			name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    			var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        		results = regex.exec(location.search);
    			return results == null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
		}

		jQuery("div.EXLBackToResults").hide();

		var vidinput = jQuery('#exlidSearchRibbon').find('input#vid');
     		jQuery(vidinput).val("LCC");

                var advancedlink = jQuery('#advancedSearchBtn');
                jQuery(advancedlink).attr('href','http://alliance-primo.hosted.exlibrisgroup.com/primo_library/libweb/action/search.do?mode=Advanced&ct=AdvancedSearch&vid=LCC&fromLogin=true');


                var browselink = jQuery('.EXLSearchFieldRibbonBrowseSearchLink').find('a.EXLSearchFieldRibbonAdvancedTwoLinks');
                jQuery(browselink).attr('href','http://alliance-primo.hosted.exlibrisgroup.com/primo_library/libweb/action/search.do?fn=showBrowse&mode=BrowseSearch&vid=LCC&fromLogin=true' );

	}





// move availability info to the right, change border and text color to green, orange, or grey color
// Anneliese Dehner
// 2015

        // iterate through brief results 
	jQuery(".EXLBriefDisplay .EXLResult").each(function(index){

                // move "view online" or "check holdings" availability info to the right 
		var avail = jQuery(this).find(".EXLResultAvailability");
		var dest = jQuery(this).find(".EXLSummaryContainer");
		jQuery(avail).appendTo(dest); 

		// add green border if resource is available 
		if (jQuery(avail).children("em").hasClass("EXLResultStatusAvailable")) {	
			jQuery(avail).css({
				"border-left-color":"#19A72A"
			});
			jQuery(avail).find("em").css({
				"color":"#19A72A"
			});

		}

                // add orange border if summit resource
		if (jQuery(avail).children("em").hasClass("EXLResultStatusMaybeAvailable")) {	
			jQuery(avail).css({
				"border-left-color":"#dd7722"
			});
			jQuery(avail).find("em").css({
				"color":"#dd7722"
			});
		}

                // add grey border if checked out
		if (jQuery(avail).children("em").hasClass("EXLResultStatusNotAvailable")) {	
			jQuery(avail).css({
				"border-left-color":"#888888"
			});
			jQuery(avail).find("span.EXLAvailabilityLibraryName").css({
				"color":"#888888"
			});
			jQuery(avail).find("span.EXLAvailabilityCollectionName").css({
				"color":"#888888"
			});
			jQuery(avail).find("span.EXLAvailabilityCallNumber").css({
				"color":"#888888"
			});
		}

        });

	// iterate through full results
	jQuery(".EXLFullView .EXLResult").each(function(index){
		var avail = jQuery(this).find(".EXLResultAvailability");

		// add green border if resource is available
		if (jQuery(avail).children("em").hasClass("EXLResultStatusAvailable")) {	
			jQuery(avail).css({
				"border-left-color":"#19A72A"
			});
			jQuery(avail).find("em").css({
				"color":"#19A72A"
			});
		}


                // add orange border if summit resource
		if (jQuery(avail).children("em").hasClass("EXLResultStatusMaybeAvailable")) {	
			jQuery(avail).css({
				"border-left-color":"#dd7722"
			});
			jQuery(avail).find("em").css({
				"color":"#dd7722"
			});
		}

		// add grey border if checked out
		if (jQuery(avail).children("em").hasClass("EXLResultStatusNotAvailable")) {	
			jQuery(avail).css({
				"border-left-color":"#888888"
			});
			jQuery(avail).find("em span.EXLAvailabilityLibraryName").css({
				"color":"#888888"
			});
			jQuery(avail).find("em span.EXLAvailabilityCollectionName").css({
				"color":"#888888"
			});
			jQuery(avail).find("em span.EXLAvailabilityCallNumber").css({
				"color":"#888888"
			});
		}

	 });
	

// wrap clickable full record metadata fields with a div and class
// stylesheet targets these to emulate buttons
// Anneliese Dehner
// 2015


	jQuery("li#Subjects-1 a").wrapAll("<div class='floating_details' />");
	jQuery("li#Subjects1 a").wrapAll("<div class='floating_details' />");
	jQuery("li#Author-1 a").wrapAll("<div class='floating_details' />");
	jQuery("li#Author-1:eq(1)").hide();


// hide "previous" and "next" in full record result navigation
// Anneliese Dehner
// 2015

	jQuery(".EXLFullView .EXLPrev").html("<img alt='Go to Previous page' src='../images/icon_arrow_prev.png' />");
	jQuery(".EXLFullView .EXLNext").html("<img alt='Go to Next page' src='../images/icon_arrow_next.png' />");



// reorder dd mm yyyy to mm dd yyyy in advanced search start & end date
// Anneliese Dehner
// 2015

	var sdaylabel = jQuery("label[for='exlidInput_drStartDay_']");
	var sdayselect = jQuery("select#exlidInput_drStartDay_");
	var smonthselect = jQuery("select#exlidInput_drStartMonth_");
	jQuery(sdaylabel).insertAfter(jQuery(smonthselect));
	jQuery(sdayselect).insertAfter(jQuery(sdaylabel));

	var edaylabel = jQuery("label[for='exlidInput_drEndDay_']");
	var edayselect = jQuery("select#exlidInput_drEndDay_");
	var emonthselect = jQuery("select#exlidInput_drEndMonth_");
	jQuery(edaylabel).insertAfter(jQuery(emonthselect));
	jQuery(edayselect).insertAfter(jQuery(edaylabel));


// add result count to scope tabs
// Anneliese Dehner (adapted from WWU, David Bass)
// 2015

function populateTabs() {
            var searchPage = document.location.href.toLowerCase().indexOf("search.do");
            if (searchPage != -1) {

                var vid = GetURLParameter("vid");
                var mode = GetURLParameter("mode");

                current_count = $.trim($("#resultsNumbersTile h1 em:first").text());        // global variable
                var api_query = "";
                var search_query = "";

                if (mode == "Advanced") {

                    var current_scope = $.trim($("#exlidSearchTabs li.selected span").attr("id"));
                        current_scope = current_scope.replace("defaultScope","");

                    $("[id^=input_freeText]").each(function(index) {
                        var thisCounter = index + 1;
                        var thisValue = $(this).val();
                        if (thisValue !== "") {
                            search_query = $(this).val();
                            var thisScopeId = "#exlidInput_scope_" + thisCounter;
                            var thisScope = $(thisScopeId + " option:selected").val();
                            var thisOperatorId = "#exlidInput_precisionOperator_" + thisCounter;
                            var thisOperator = $(thisOperatorId + " option:selected").val();
                            api_query += "&q[]=" + thisScope + "," + thisOperator + "," + escape(search_query);
                        }
                    });
                } else {
                    var current_scope = $.trim($(".EXLSelectedScopeId").val());
                    search_query = $("#search_field").val();
                    api_query = "&q=any,contains," + escape(search_query);
                }

                if (search_query != "") {

                    // append the query to the adv_search button
                    $('#btn_adv_search').attr('href', function() {
                        return this.href + '&vid=' + vid + '&vl(freeText0)=' + search_query + "&";
                    });

                    $("#exlidTab0 a").append(" <span class='spin-container'> &nbsp; &nbsp; <span class='spin'> &nbsp; </span> </span>");
                    $("#exlidTab1 a").append(" <span class='spin-container'> &nbsp; &nbsp; <span class='spin'> &nbsp; </span> </span>");
                    $("#exlidTab2 a").append(" <span class='spin-container'> &nbsp; &nbsp; <span class='spin'> &nbsp; </span> </span>");
 

                    function getCount(tabId, ajax_query, scope) {
                        ajax_query = ajax_query.replace(/%2C/g, '+');       // replace comma with plus
                        var count_url = "http://library.lclark.edu/primo/scope-count.php?s=" + scope + ajax_query + "&callback=?";
                        
		
                        var thisId = "#" + tabId;

                        $.getJSON(count_url, function(data) {
			  
                            $(thisId + " span.spin-container .spin").fadeOut("fast");
                            $(thisId + " span.spin-container").addClass("fadein").text("(" + data.count + ")");
                        }).done(function( json ) {
                            // console.log(json);
				
                        }).fail(function( jqxhr, textStatus, error ) {
                            var err = textStatus + ", " + error;
                            console.log( "Request Failed: " + err );
                        });
                    }

                    var selected_tab = $("#tab").val();

                    // LCC
                    if (selected_tab == "lc_only") {
                        $("#exlidTab2 span.spin-container").addClass("fadein").text("(" + current_count + ")");
                    } else {
                        getCount("exlidTab2", api_query, "lcc_local");
                    }

                    // LCC + Summit
                    if (selected_tab == "lc_summit") {
                        $("#exlidTab1 span.spin-container").addClass("fadein").text("(" + current_count + ")");
                        
                    } else {
                        getCount("exlidTab1", api_query, "local_nz");
                    }

                    // LCC + Summit + Articles
                    if (selected_tab == "default_tab") {
                        $("#exlidTab0 span.spin-container").addClass("fadein").text("(" + current_count + ")");
                    } else {
                        getCount("exlidTab0", api_query, "lcc_nz_pc");
                    }

                }
            }
        }



var populateTabs = populateTabs();


// worldcat search button (for institution with one worldcat instance)
// Anneliese Dehner (adapted from Notre Dame and Northeastern University code)
// 2015

var worldCatBaseUrl = 'http://watzek.on.worldcat.org/search?queryString=' ;
      if( worldcatLogo === undefined){
        var worldcatLogo = 'http://alliance-primo.hosted.exlibrisgroup.com/primo_library/libweb/uploaded_files/LCC_TEST/worldcat-logo.png';
      }
      function getWCIndex(exSearch){
        switch (exSearch){
        case 'any':
          return 'kw';
        case 'title':
          return 'ti';
        case 'creator':
          return 'au';
        case 'sub':
          return 'su';
        case 'isbn':
          return 'bn';
        case 'issn':
          return 'n2';
        default:
          return 'kw';
        }

      }

      //declaring the variables before if/else
      var $link;
      var searchString = '';
      var $searchField = $('#search_field');
      var searchTerm = $searchField.val() || '';
      

     if ($('#exlidAdvancedSearchRibbon').length && !$('#exlidAdvancedSearchRibbon').hasClass('EXLAdvancedBrowseRibbon')){

        //loop through each advanced search row
        $('.EXLAdvancedSearchFormRow').each(function(index) {
             
          	//as long as free text is entered
          	if( ($('#input_freeText' + index).length) && ($('#input_freeText' + index).val() !== '')){
            		//the dropdowns start with 1
            		var ddIndex = index + 1;
            		//convert the search type from what's in the dropdown
            		//to worldcat's term
            		var wcIndex = getWCIndex($('#exlidInput_scope_' + ddIndex).val());

            		//for 'contains'search
            		var operator = '%3A';

            		//for 'exact' search
            		if ($('#exlidInput_precisionOperator_' + ddIndex).val() === 'exact'){
              			operator = '%3D';
            		}

            		//construct search string
            		searchString += wcIndex + operator + $('#input_freeText' + index).val() + ' ';
          	}

                      
         });


        //also language search
        if (($("#exlidInput_language_").length > 0) && ($('#exlidInput_language_').val() !== 'all_items')){
          searchString += 'ln' + '%3A' + $('#exlidInput_language_').val() + ' ';
        }



        //also material type search
	if (($("#exlidInput_mediaType_").length > 0) && ($('#exlidInput_mediaType_').val() !== 'all_items')){
                    var mediatype = '';
                    if($('#exlidInput_mediaType_').val() == 'journals'){
                           mediatype = 'jrnl';
                    } else if ($('#exlidInput_mediaType_').val() == 'articles'){
                           mediatype = 'artchap';
                    } else if ($('#exlidInput_mediaType_').val() == 'ebooks'){
                           mediatype = 'book+%2B+x4%3Adigital';
                    } else if ($('#exlidInput_mediaType_').val() == 'pbooks'){
                           mediatype = 'book';
                    } else if ($('#exlidInput_mediaType_').val() == 'audio_video'){
                           mediatype = 'video+OR+x0%3Aaudio';
                    } else {
                           mediatype = $('#exlidInput_mediaType_').val();
                    }

          searchString += 'x0' + '%3A' + mediatype + ' ';
        }

        //also start date search
        if (($("#input_873667772UI6").length > 0) && ($("#input_873667772UI6").val() !== 'year')){
                searchString += 'yr' + '%3A' + $('#input_873667772UI6').val() + '';  	
         }

        //also end date search
        if (($("#input_873667775UI6").length > 0) && ($("#input_873667775UI6").val() !== 'year')){
                 searchString += '..' + $("#input_873667775UI6").val() + ' ';
        } else if (($("#exlidInput_publicationDate_").length > 0) && ($('#exlidInput_publicationDate_').val() !== 'all_items')){
                 var publicationDate = '';
                 curr=new Date().getFullYear();
                 if($('#exlidInput_publicationDate_').val() == '1-YEAR'){
                           publicationDate = curr-1 + '..' + curr + ' ';
                    } else if($('#exlidInput_publicationDate_').val() == '2-YEAR'){
                           publicationDate = curr-2 + '..' + curr + ' ';
                    } else if($('#exlidInput_publicationDate_').val() == '5-YEAR'){
                           publicationDate = curr-5 + '..' + curr + ' ';
                    } else if($('#exlidInput_publicationDate_').val() == '10-YEAR'){
                           publicationDate = curr-10 + '..' + curr + ' ';
                    } else if($('#exlidInput_publicationDate_').val() == '20-YEAR'){
                           publicationDate = curr-20 + '..' + curr + ' ';
                    } else {publicationDate ='';}

                 searchString += 'yr' + '%3A' + publicationDate + ' ';
        }




        if (searchString !== ''){
          	//replace space with +
          	searchString = searchString.replace(/ /g, '+');

          	//remove last "+"
          	searchString = searchString.substring(0,searchString.length-1);

          	//escape quotes
          	searchString = searchString.replace(/\"/g, '&quot;');
          	searchString = searchString.replace(/\'/g, "\\'");

          	$link = '<a onclick="javascript:window.open(\''+ worldCatBaseUrl + searchString + '\');" href="javascript:void(0);" class="navbar-link" title="Search WorldCat for more results.">Search <img src="'+ worldcatLogo +'" width="22" height="22" alt=" "> WorldCat</a>';
          	//add the worldcat link
          	$('.EXLSearchFieldRibbonFormLinks').append($link);
        }

      } else if ( searchTerm.length > 0 ){
          
          //escape quotes
          searchTerm = searchTerm.replace(/\"/g, '&quot;');
          searchTerm = searchTerm.replace(/\'/g, "\\'");
          $link = '<a onclick="javascript:window.open(\''+ worldCatBaseUrl + searchTerm + '\');" href="javascript:void(0);" class="navbar-link" title="Search Other Libraries for ' + searchTerm + '">Search <img src="'+ worldcatLogo +'" width="22" height="22" alt=" "> WorldCat</a>';
        

          //add the worldcat links
          jQuery('.EXLSearchFieldRibbonAdvancedSearchLink').before($link);
          $link.wrap('<div class="EXLSearchFieldRibbonAdvancedSearchLink"/>');

      } else {
        return false;
      }



// worldcat search button (for institution with multiple worldcat instances) 
// dependent on worldcat.php which processes link from Primo to Worldcat. Since we have two libraries with different WC instances, 
// checks user IP, and forwards to appropriate WC instance if on campus. Otherwise, presents user with option, and sets cookie based upon the user's choice.
// Anneliese Dehner & Jeremy McWilliams (adapted from Notre Dame and Northeastern University code)
// 2015 

/*var worldCatBaseUrl ='http://library.lclark.edu/primo/worldcat.php?queryString=';


	if( worldcatLogo === undefined){
        	var worldcatLogo = 'http://alliance-primo.hosted.exlibrisgroup.com/primo_library/libweb/uploaded_files/LCC_TEST/worldcat-logo.png';
      	}
      	function getWCIndex(exSearch){
        	switch (exSearch){
        		case 'any':
          			return 'kw';
        		case 'title':
          			return 'ti';
        		case 'creator':
          			return 'au';
        		case 'sub':
          			return 'su';
        		case 'isbn':
          			return 'bn';
        		case 'issn':
          			return 'n2';
        		default:
          			return 'kw';
        		}

      		}

      		//declaring the variables before if/else
      		var $link;
      		var searchString = '';
      		var $searchField = $('#search_field');
      		var searchTerm = $searchField.val() || '';
		var view=GetURLParameter('vid');
      		if ($('#exlidAdvancedSearchRibbon').length && !$('#exlidAdvancedSearchRibbon').hasClass('EXLAdvancedBrowseRibbon') && view != 'LCC_RESERVES'){

        		//loop through each advanced search row
        		$('.EXLAdvancedSearchFormRow').each(function(index) {
             
          		//as long as free text is entered
          		if( ($('#input_freeText' + index).length) && ($('#input_freeText' + index).val() !== '')){
            			//the dropdowns start with 1
            			var ddIndex = index + 1;
            			//convert the search type from what's in the dropdown
            			//to worldcat's term
            			var wcIndex = getWCIndex($('#exlidInput_scope_' + ddIndex).val());
            			//for 'contains'search
            			var operator = '%3A';
            			//for 'exact' search
            			if ($('#exlidInput_precisionOperator_' + ddIndex).val() === 'exact'){
              				operator = '%3D';
            			}
            			//construct search string
            			searchString += wcIndex + operator + $('#input_freeText' + index).val() + ' ';
          		}     
         	});

        	//also language search
        	if (($("#exlidInput_language_").length > 0) && ($('#exlidInput_language_').val() !== 'all_items')){
          		searchString += 'ln' + '%3A' + $('#exlidInput_language_').val() + ' ';
        	}

	        //also material type search
		if (($("#exlidInput_mediaType_").length > 0) && ($('#exlidInput_mediaType_').val() !== 'all_items')){
                    	var mediatype = '';
                    	if($('#exlidInput_mediaType_').val() == 'journals'){
                           	mediatype = 'jrnl';
                    	} else if ($('#exlidInput_mediaType_').val() == 'articles'){
                           	mediatype = 'artchap';
                    	} else if ($('#exlidInput_mediaType_').val() == 'ebooks'){
                           	mediatype = 'book+%2B+x4%3Adigital';
                    	} else if ($('#exlidInput_mediaType_').val() == 'pbooks'){
                           	mediatype = 'book';
                    	} else if ($('#exlidInput_mediaType_').val() == 'audio_video'){
                           	mediatype = 'video+OR+x0%3Aaudio';
                    	} else {
                           	mediatype = $('#exlidInput_mediaType_').val();
                    	}
          		searchString += 'x0' + '%3A' + mediatype + ' ';
        	}

        	//also start date search
        	if (($("#input_873667772UI6").length > 0) && ($("#input_873667772UI6").val() !== 'Year')){
                	searchString += 'yr' + '%3A' + $('#input_873667772UI6').val() + '';  	
         	}

        	//also end date search
        	if (($("#input_873667775UI6").length > 0) && ($("#input_873667775UI6").val() !== 'Year')){
                 	searchString += '..' + $("#input_873667775UI6").val() + ' ';
        	} else if (($("#exlidInput_publicationDate_").length > 0) && ($('#exlidInput_publicationDate_').val() !== 'all_items')){
                 	var publicationDate = '';
                 	curr=new Date().getFullYear();
                 	if($('#exlidInput_publicationDate_').val() == '1-YEAR'){
                        	publicationDate = curr-1 + '..' + curr + ' ';
                    	} else if($('#exlidInput_publicationDate_').val() == '2-YEAR'){
                           	publicationDate = curr-2 + '..' + curr + ' ';
                    	} else if($('#exlidInput_publicationDate_').val() == '5-YEAR'){
                           	publicationDate = curr-5 + '..' + curr + ' ';
                    	} else if($('#exlidInput_publicationDate_').val() == '10-YEAR'){
                           	publicationDate = curr-10 + '..' + curr + ' ';
                    	} else if($('#exlidInput_publicationDate_').val() == '20-YEAR'){
                           	publicationDate = curr-20 + '..' + curr + ' ';
                    	} else {publicationDate ='';}
                 	searchString += 'yr' + '%3A' + publicationDate + ' ';
        	}
        	if (searchString !== ''){
          		//replace space with +
          		searchString = searchString.replace(/ /g, '+');

          		//remove last "+"
          		searchString = searchString.substring(0,searchString.length-1);

          		//escape quotes
          		searchString = searchString.replace(/\"/g, '&quot;');
          		searchString = searchString.replace(/\'/g, "\\'");
          		
          		$link = '<a onclick="javascript:window.open(\''+ worldCatBaseUrl + searchString + '\');" href="javascript:void(0);" class="navbar-link" title="Search WorldCat for more results.">Search <img src="'+ worldcatLogo +'" width="22" height="22" alt=" "> WorldCat</a>';
			
          		//add the worldcat link
          		$('.EXLSearchFieldRibbonFormLinks').append($link);
        	}

      } else if ( searchTerm.length > 0 && view !='LCC_RESERVES'){
           
          	//escape quotes
          	searchTerm = searchTerm.replace(/\"/g, '&quot;');
          	searchTerm = searchTerm.replace(/\'/g, "\\'");
          	$link = '<a onclick="javascript:window.open(\''+ worldCatBaseUrl + searchTerm + '\');" href="javascript:void(0);" class="navbar-link" title="Search Other Libraries for ' + searchTerm + '">Search <img src="'+ worldcatLogo +'" width="22" height="22" alt=" "> WorldCat</a>';
        
          	//add the worldcat links
          	jQuery('.EXLSearchFieldRibbonAdvancedSearchLink').before($link);
          	//$link.wrap('<div class="EXLSearchFieldRibbonAdvancedSearchLink"/>');
          	

          	
      } else {
     
      
        	return false;
      }*/





    
// permalink construction 2015
// jeremy mcwilliams
// 13nov2013 - show a short url for the basic search results 

	
	

	var searchPage = document.location.href.toLowerCase().indexOf("search.do");
	if (searchPage != -1) {

		// do not show on the browse search page
		var fn = GetURLParameter('fn');        // conjunction conjunction, what's your function?
		var mode=GetURLParameter('mode');
		var view=GetURLParameter('vid');
		if (fn !== "BrowseRedirect" && mode !="Advanced" && view !="LCC_RESERVES") {

			// do not create a permalink if src = permalink
		        var src = GetURLParameter('src');
		                  
			// if (src !== "permalink") {
				var search_query = $("#search_field").val();
		        	if (search_query != "") {
		     			var scope_url = GetURLParameter('scp.scps');
		                        // console.log(scope_url);
		                var search_scope_url = GetURLParameter('search_scope');
		                var clean_scope = "";
		                if ((scope_url == "scope%3A%28LCC%29") ||  (search_scope_url == "lcc_local")) {
		                    scope = "/lcc_local";
		                } else if ((scope_url == "scope%3A%28NZ%29%2Cscope%3A%28LCC%29")  || (search_scope_url == "local_nz")) {
		                    scope = "/local_nz";
		                } else if ((scope_url == "scope%3A%28NZ%29%2Cscope%3A%28LCC%29%2Cprimo_central_multiple_fe")  || (search_scope_url == "lcc_nz_pc")) {
		                    scope = "/lcc_nz_pc";
		                } else {
		                    scope = "";     // Everything;
		                }
		                
		                if (scope==""){
		                
		                	var tab_url = GetURLParameter('tab');
		                	//alert(tab_url);
		                	if (tab_url=="default_tab"){scope="/lcc_nz_pc";}
		                	if (tab_url=="lc_only"){scope="/lcc_local";}
		                	if (tab_url=="lc_summit"){scope="/local_nz";}
		                	if (tab_url=="lc_summit_articles"){scope="/lcc_nz_pc";}
		                
		                
		                }
		                

		                
						search_query = search_query.split(' ').join('+');       // replace spaces with plus
		                search_query = search_query.replace(/\//g, "%252F");    // replace slashes with special character that will be converted back to slashes in the php redirector code
		                var permaLink = "http://library.lclark.edu/primo/" + search_query + scope;
				        facets=getFacets();
                       if(facets.length>0){ permaLink += "/"+facets;}
		                var permaLinkPretty = "<div title='permaLink - share this URL with others' id='permalinkPretty'><div><span class='disable-select'>Permalink for this search: </span><span class='perma-link'>" + permaLink + "</span> <span class='display-block'> </span></div></div>";
		                $("#exlidHeaderContainer").append(permaLinkPretty);
		           	}
			// }
		}
	}


	//  create a permalink on the display page for a specific item 
	
	var displayPage = document.location.href.toLowerCase().indexOf("display.do");

	if (displayPage != -1) {
	
	
			// only run this code on the display page for now; do not run in search results page
	        // select the View It tab (instead of the Details tab) when the display.do page loads 
	      
	        // $("ul.EXLResultTabs li:first").addClass("EXLResultSelectedTab");
	        // $("#exlidResult0-TabContainer-viewOnlineTab").removeClass("EXLResultTabContainerClosed");
	        
	        // Primo 4.6 introduced item-level permalinks, so let's use those instead of the homegrown permalinks
	        var exli_permaLink = $(".EXLButtonSendToPermalink").find("a").attr("href");
	        // this should return something like
	        // "permalink.do?docId=TN_springer_jour10.2165/00003495-200161130-00006&vid=WWU&fn=permalink"
	        exli_permaLink = exli_permaLink.split('?');     // split the exli_permalink at the question mark
	        exli_permaLink = exli_permaLink[1];             // use what follows the question mark
	        var docId = GetParameter(exli_permaLink,"docId");      // extract the docId value from that string
	        var vid = GetParameter(exli_permaLink,"vid");           // extract the vid value from the exli_permalink string
	        var exli_permaLink_final = "http://primo.lclark.edu/" + vid + ":" + docId;     // put the pieces together
	        var permaLink = "<span class='wwu_permalink'>" + exli_permaLink_final + "</span>";
	        var permaLinkPretty = "<div title='permaLink - share this URL with others' id='permalinkPretty' class='permalinkPretty-displayPage'><div><span class='disable-select'>Permalink for this item: </span><span class='perma-link'>" + permaLink + "</span> <span class='display-block'> </span></div></div>";
	        $("#exlidHeaderContainer").append(permaLinkPretty);
	}



/*
	var displayDlPage = document.location.href.toLowerCase().indexOf("dlDisplay.do");
	console.log("displayDlPage:"+displayDlPage);
	
	if (displayDlPage != -1) {
	
	
			// only run this code on the display page for now; do not run in search results page
	        // select the View It tab (instead of the Details tab) when the display.do page loads 
	      
	        // $("ul.EXLResultTabs li:first").addClass("EXLResultSelectedTab");
	        // $("#exlidResult0-TabContainer-viewOnlineTab").removeClass("EXLResultTabContainerClosed");
	        
	        // Primo 4.6 introduced item-level permalinks, so let's use those instead of the homegrown permalinks
	        var exli_permaLink = $(".EXLButtonSendToPermalink").find("a").attr("href");
	        // this should return something like
	        // "permalink.do?docId=TN_springer_jour10.2165/00003495-200161130-00006&vid=WWU&fn=permalink"
	        exli_permaLink = exli_permaLink.split('?');     // split the exli_permalink at the question mark
	        exli_permaLink = exli_permaLink[1];             // use what follows the question mark
	        var docId = GetParameter(exli_permaLink,"docId");      // extract the docId value from that string
	        var vid = GetParameter(exli_permaLink,"vid");           // extract the vid value from the exli_permalink string
	        var exli_permaLink_final = "http://primo.lclark.edu/" + vid + ":" + docId;     // put the pieces together
	        var permaLink = "<span class='wwu_permalink'>" + exli_permaLink_final + "</span>";
	        var permaLinkPretty = "<div title='permaLink - share this URL with others' id='permalinkPretty' class='permalinkPretty-displayPage'><div><span class='disable-select'>Permalink for this item: </span><span class='perma-link'>" + permaLink + "</span> <span class='display-block'> </span></div></div>";
	        $("#exlidHeaderContainer").append(permaLinkPretty);
	}

*/

	
	//  create a permalink on the openurl page for a specific item 
		
	var vid = GetURLParameter("vid");
	if (vid == "lcc_services_page"){
	        
	        // Primo 4.6 introduced item-level permalinks, so let's use those instead of the homegrown permalinks
	        var exli_permaLink_final = $(".EXLButtonSendToOpenUrl").find("a").attr("href");
		console.log(exli_permaLink_final);
	        var permaLink = "<span class='wwu_permalink'>" + exli_permaLink_final + "</span>";
	        var permaLinkPretty = "<div title='permaLink - share this URL with others' id='permalinkPretty' class='permalinkPretty-displayPage'><div><span class='disable-select'>Permalink for this item: </span><span class='perma-link'>" + permaLink + "</span> <span class='display-block'> </span></div></div>";
	        $("#exlidHeaderContainer").append(permaLinkPretty);
	}




}); /* end document ready */


/* function to get and display permalink for */

function dlPlink(){

	//console.log("plink!");

	var displayDlPage = document.location.href.toLowerCase().indexOf("dldisplay.do");
	//console.log("displayDlPage:"+displayDlPage);
	
	if (displayDlPage != -1) {
	
	
			// only run this code on the display page for now; do not run in search results page
	        // select the View It tab (instead of the Details tab) when the display.do page loads 
	      
	        // $("ul.EXLResultTabs li:first").addClass("EXLResultSelectedTab");
	        // $("#exlidResult0-TabContainer-viewOnlineTab").removeClass("EXLResultTabContainerClosed");
	        
	        // Primo 4.6 introduced item-level permalinks, so let's use those instead of the homegrown permalinks
	        var exli_permaLink = $(".EXLButtonSendToPermalink").find("a").attr("href");
	        // this should return something like
	        // "permalink.do?docId=TN_springer_jour10.2165/00003495-200161130-00006&vid=WWU&fn=permalink"
	        exli_permaLink = exli_permaLink.split('?');     // split the exli_permalink at the question mark
	        exli_permaLink = exli_permaLink[1];             // use what follows the question mark
	        var docId = GetParameter(exli_permaLink,"docId");      // extract the docId value from that string
	        var vid = GetParameter(exli_permaLink,"vid");           // extract the vid value from the exli_permalink string
	        var exli_permaLink_final = "http://primo.lclark.edu/" + vid + ":" + docId;     // put the pieces together
	        var permaLink = "<span class='wwu_permalink'>" + exli_permaLink_final + "</span>";
	        var permaLinkPretty = "<div title='permaLink - share this URL with others' id='permalinkPretty' class='permalinkPretty-displayPage'><div><span class='disable-select'>Permalink for this item: </span><span class='perma-link'>" + permaLink + "</span> <span class='display-block'> </span></div></div>";
	        $("#exlidHeaderContainer").append(permaLinkPretty);
	}



}


/*  displays permalink in resolver view  */
function resolverPlink(){


	//console.log(" resolver plink!");

	var displayDlPage = document.location.href.toLowerCase().indexOf("openurl");
	//console.log("displayDlPage:"+displayDlPage);
	
	if (displayDlPage != -1) {
	
	
			// only run this code on the display page for now; do not run in search results page
	        // select the View It tab (instead of the Details tab) when the display.do page loads 
	      
	        // $("ul.EXLResultTabs li:first").addClass("EXLResultSelectedTab");
	        // $("#exlidResult0-TabContainer-viewOnlineTab").removeClass("EXLResultTabContainerClosed");
	        
	        // Primo 4.6 introduced item-level permalinks, so let's use those instead of the homegrown permalinks
	        //var exli_permaLink = $(".EXLButtonSendToOpenUrl").find("a").attr("href");
	        
	        var exli_permaLink=document.location.href;
	        
	        var pLink=exli_permaLink.replace("alliance-primo.hosted.exlibrisgroup.com", "primo.lclark.edu");
	        var mode=$('#mode').val();
	        //console.log("MODE: "+mode);
	        
	        var box=$(".EXLButtonSendToOpenUrl a").attr('onclick');
	        var c=box.split(";");
	        var b=c[1];
	        var d=b.split("'");
	        
	        recordIndex=d[1];
	        recordId=d[3];

			action='almaServiceOpenUrl';
			fn='';
			additionalParameters=null;
			var timestamp=new Date().getTime();
			url=action+".do"+"?fn="+fn+"&ts="+timestamp+additionalParameters;
			urlParameters='recId='+recordId+'&recIdx='+recordIndex;
			url=url+"&mode="+mode+"&"+urlParameters;


			/* grabbed from the Primo openPrimoLightBox() function*/
			/* permalink in uresolver is only available via ajax....hence the below */
			var data;
			$.ajax(
				{url:url,
				data:data,
				dataType:'xml',
				global:false,
				beforeSend:function(request){setAjaxRequestHeader(request);},
				error:function(request,errorType,exceptionOcurred){
					if(errorType=='timeout'){notifyAjaxTimeout();}
					else{generalAjaxError();}
					document.getElementById('exliLoadingFdb').style.display='none';
					$('#exliLoadingFdb').css('top',0);
					document.getElementById('exliGreyOverlay').style.display='none';
					document.getElementById('exliWhiteContent').style.display='none';
					return false;
				},
				success:function(data){

					var elm=$(data).find("openUrlResultsXml");
					var cdata=$(elm).html();
					var $elem = $('<div>').html(cdata);
					var $img = $elem.find('input').val();
					var pLink=$img;
					var permaLink = "<span class='wwu_permalink'>" + pLink + "</span>";
	        		var permaLinkPretty = "<div title='permaLink - share this URL with others' id='permalinkPretty' class='permalinkPretty-displayPage'><div><span class='disable-select'>Permalink for this item: </span><span class='perma-link'>" + permaLink + "</span> <span class='display-block'> </span></div></div>";
	        		$("#exlidHeaderContainer").append(permaLinkPretty);

			
				}
			}
			).done(function() {
  				
  					/* fire click/select function*/
  				    $('span.perma-link').click(function() {

        				SelectText("perma-link");

    				});
			});	

	}




}




        

// permalink functions
function GetParameter(string, sParam) {
        // thanks to http://jquerybyexample.blogspot.com/2012/06/get-url-parameters-using-jquery.html
        var sPageURL = string;
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
        	var sParameterName = sURLVariables[i].split('=');
       		if (sParameterName[0] == sParam) {
                	return sParameterName[1];
            	}
	}
}


function GetURLParameter(sParam) {
        // thanks to http://jquerybyexample.blogspot.com/2012/06/get-url-parameters-using-jquery.html
        var sPageURL = window.location.search.substring(1);
        var sURLVariables = sPageURL.split('&');
        for (var i = 0; i < sURLVariables.length; i++) {
        	var sParameterName = sURLVariables[i].split('=');
        	if (sParameterName[0] == sParam) {
            		return sParameterName[1];
           	}
	}
}


function getFacets(){
	facs=""
	facetVals=[];
	jQuery(".EXLRemoveRefinement").each(function(index){
	
		var att=jQuery(this).children("a").children("strong").attr("class");
		var lab=jQuery(this).children("a").children("strong").text();
		var f = att.replace("EXLSearchRefinement", "");
		//alert(f);
		F=facetMap(f)
		//alert(lab);
		facetVals[f]=lab;
		nv=F+":"+lab+"|";
    	facs +=nv;
    	
	});
	facs = facs.substring(0, facs.length - 1);
	//alert(facs);
	return facs;
	

}

function facetMap(arg){

    var facetMap=[];
    facetMap["facet_rtype"]="resource type";
    //facetMap["facet_tlevel"]="show only";
    facetMap["facet_tlevel"]="top level";
    facetMap["facet_creator"]="Creator";
    facetMap["facet_creationdate"]="creation date";
    facetMap["facet_creator"]="Creator";
    facetMap["facet_lang"]="language";
    facetMap["facet_jtitle"]="journal title";
    facetMap["facet_lcc"]="LCC Classification";
    facetMap["facet_domain"]="classification";
    facetMap["facet_local3"]="Physical Location";
    facetMap["facet_local4"]="library";
    facetMap["facet_topic"]="Topic";
    
    val=facetMap[arg];
    return val;
    
} 

function SelectText(element) {
    var doc = document
        , text = doc.getElementsByClassName(element)
        , range, selection
    ;    
    if (doc.body.createTextRange) {
        range = document.body.createTextRange();
        range.moveToElementText(text);
        range.select();
    } else if (window.getSelection) {
        selection = window.getSelection();        
        range = document.createRange();
        range.selectNodeContents(text[0]);
        selection.removeAllRanges();
        selection.addRange(range);
    }
}

$(function() {
    $('span.perma-link').click(function() {

        SelectText("perma-link");

    });
});



