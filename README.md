# README #

This repository includes files used to customize the interface of Primo (by Ex Libris) at primo.lclark.edu. 

Please note that these customizations require that you use the tabbed scope interface in the PBO; it will not work with the standard select-list scope list.

To use this in your environment, we recommend that you create a view (using the Primo Back Office, or PBO), and load (or point to) these files. 

### Features ###

1. Scope tabs with search result totals
2. Search WorldCat button
3. Prompt to sign in
4. Permalink for simple/brief search results
5. Item availability is aligned right
6. Flipped the order of day and month in the Advanced Search date inputs (from DD-MM-YYYY to MM-DD-YYYY)
7. Permalink for full record display
8. Actions menu popped out and aligned left
9. Get it, View it, Details, and Virtual Browse tabs look like clickable tabs
10. Linked metadata fields (author and subject) display as buttons


![Screen Shot 2015-08-19 at 9.48.45 AM.png](https://bitbucket.org/repo/yb7Edx/images/976856451-Screen%20Shot%202015-08-19%20at%209.48.45%20AM.png)

![Screen Shot 2015-08-19 at 10.01.50 AM.png](https://bitbucket.org/repo/yb7Edx/images/3384372426-Screen%20Shot%202015-08-19%20at%2010.01.50%20AM.png)

### How do I get set up? ###

* Create a view with tabbed scopes in the PBO
* Edit html, css, js, php, and htaccess files with specs for your view and institution
* Upload files to the PBO for this view and/or upload files to your server
* Permalink Dependencies: access.htaccess, index.php
* Tab Scope Dependencies: scope-count.php
* Search WorldCat Button Dependencies (for institutions with multiple WorldCat instances. If you only have one WorldCat instance, this file is not necessary): worldcat.php

### Who do I talk to? ###

* Anneliese Dehner or Jeremy McWilliams
* jeremym at lclark dot edu