<?php
header("Cache-Control: no-store, no-cache, must-revalidate, max-age=0");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

	/*
		copyright 2014 WWU Libraries
		license: MIT
		version 1.0
		author: David Bass
		summary: redirect short URL to Primo
		example:
			http://library.wwu.edu/onesearch/bellingham/wwu_only will be redirected to
			http://onesearch.library.wwu.edu/primo_library/libweb/action/dlSearch.do?institution=WWU&vid=WWU&query=any,contains,bellingham&search_scope=Books&src=permalink

		todo:
			- facets
			- sort-by
			- advanced search
			- make scopes configuration easier
	*/

	/* this script relies on an .htacess file 	*/


	$facetMap=array(
		"resource type"=>"facet_rtype",
		"show only"=>"facet_tlevel",
		"creator"=>"facet_creator",
		"creation date"=>"facet_creationdate",
		"language"=>"facet_lang",
		"journal title"=>"facet_jtitle",
		"collection"=>"facet_domain",
		"LCC Classification"=>"facet_lcc",
		"physical location"=>"facet_local3",
		"library"=>"facet_local4"
	);



/*
	$facetMap=array(
	
		"facet_rtype"=>"resource type",
		"facet_tlevel"=>"show only",
		"facet_creator"=>"creator",
		"facet_creationdate"=>"creation date",
		"facet_lang"=>"language",
		"facet_jtitle"=>"journal title",
		"facet_domain"=>"collection",
		"facet_lcc"=>"classification",
		"facet_local3"=>"physical location",
		"facet_local4"=>"library",
		"facet_topic"=>"topic"
	);

  */



	
	$valueMap=array(
		'online resources'=>'online_resources$$ILCC',
		'%20online%20resources'=>'online_resources$$ILCC',
		
		'Available in the Library'=>'available$$ILCC',
		'%20Available%20in%20the%20Library'=>'available$$ILCC',
		
		'Peer-reviewed Journals'=>'peer_reviewed',
		'%20Peer-reviewed%20Journals'=>'peer_reviewed'
	);


$libraries=array(
	"%20Boley%20Law%20Library"=>"Boley+Law+Library%24%24ILCC",
	"%20Watzek%20Library"=>"Watzek+Library%24%24ILCC",
	"Watzek Library"=>"Watzek+Library%24%24ILCC",
	"Boley Law Library"=>"Boley+Law+Library%24%24ILCC"


);

$resourceTypes=array(
	"eBooks"=>"books",
	"Print Books"=>"pbooks",
	"Audio CDs"=>"audio_CDs",
	"Government Documents"=>"government_documents",
	"DVD Videos"=>"dvd_videos",
	"Microform"=>"microform",
	"Journals"=>"journals",
	"Videocassettes"=>"videocassettes",
	"Scores"=>"scores",
	"Theses & Dissertations"=>"dissertations",
	"LPs"=>"lprecords",
	"Loose-Leaf"=>"looseleaf",
	"Blu-ray Videos"=>"blurayvideos",
	"Computer Files"=>"computerfiles",
	"Audiocassettes"=>"audiocassettes",
	"Filmstrips"=>"filmstrips",
	"CD-ROM/DVD-ROM"=>"cddvdrom",
	"Video Games"=>"videogames",
	"Kits"=>"kits",
	"Annuals & Otehr Serials not in a Specific Category"=>"annual",
	"Newspapers"=>"newspapers",
	"Three-Dimensional Objects"=>"threedobject",
	"Films"=>"films",
	"LaserDiscs"=>"laserdiscs",
	"Spoken Audio Discs & Cassettes"=>"spokenaudio",
	"Research Datasets"=>"research_datasets",
	"Audio Visual"=>"audio_video",
	"Video"=>"video",
	"Reference Entries"=>"reference_entrys",
	"eAudio & eVideo"=>"media",
	"Maps"=>"maps",
	"Articles"=>"articles",
	"All Text"=>"alltext",
	"Images"=>"images",
	"Newspaper Articles"=>"newspaper_articles",
	"Technical Report"=>"technical_reports",
	"Conference Proceedings"=>"conference_proceedints",
	"Databases"=>"databases",
	"Journals"=>"journals",
	"Other"=>"other",
	"Text Resources"=>"text_resources",
	"Web Sites"=>"websites",
	"All Items"=>"allitems",
	"Legal Documents"=>"legal_documents",
	"Musical Audio Discs & Cassettes"=>"musicalaudio",
	"Physical File & Video"=>"pvideo",
	"Archived Materials"=>"mixedmat"
);


$physicalLocations=array(

	"Gov. Docs Fiche"=>"Gov. Docs Fiche$$ILCC",





);









	$url_array = parse_url($_SERVER['REQUEST_URI']);
	$path = isset($url_array['path']) ? $url_array['path'] : '';
    
            $path=str_replace("%7C","|",$path);
       // echo $path;
	$path_parts = explode("/", $path);
	
	//var_dump($path_parts);
	
	
    $query = "";
    $scope = "&search_scope=All";

    $source = "&src=permalink";     // in case there is no referrer
    if(isset($_SERVER['HTTP_REFERER'])) {
		# the referrer can be really long; if so, lets just get the hostname;
		$referrer = $_SERVER['HTTP_REFERER'];
		$src = parse_url($referrer, PHP_URL_HOST);
        $src = str_replace('.', '_', $src);
		$source = "&src=" . urlencode($src);

        
    }

	/*
	echo "<pre>";
	var_dump($path_parts);
	echo "</pre>";
	*/

	if ($path_parts[2] != "") {
        # the search query in this example is "underground+newspaper+collection"
        # http://library.wwu.edu/onesearch/underground+newspaper+collection
		$query = $path_parts[2];
        $query = str_replace('%252F', '/', $query);     // replace underscores with slash, because slashes were replaced with underscores in javascript when permalink was generated (to bypass Apache rewriterule issue)

        // if this is a doi, we need to remove the "doi:" from the start of the query and surround the query with quotes for Primo to be able to find it;
        $doi = strrpos($query, "DOI:");
        if ($doi !== false) {
            $query_parts = explode("-author:", $query);
            $doi = $query_parts[0];
            $author = $query_parts[1];

            $query = str_replace('DOI:', '"', $doi) . '" ';     // remove the 'DOI:' and surround the doi with quotes and a trailing space
            $query .= str_replace('-author:', '', $author);     // remove '-author:' from the query
        }

	} else {
		echo "Search query was empty.";
		exit();
	}

	if ($path_parts[3] != "") {
        # the scope in the following example URL is "At WWU Only"
            # http://library.wwu.edu/onesearch/underground+newspaper+collection/wwu_only
        # here the scope is "WWU Libraries + Summit"
            # http://library.wwu.edu/onesearch/underground+newspaper+collection/wwu_summit

        $scope = $path_parts[3];
        if ($scope == "lcc_local") {
            $scope = "&search_scope=lcc_local";
        } elseif ($scope == "local_nz") {
            $scope = "&search_scope=local_nz";
        } elseif ($scope == "lcc_nz_pc") {
            $scope = "&search_scope=lcc_nz_pc";
        }
    }
    
    
    /*
    new, for facets
    
    test link: http://library.lclark.edu/primo/a+wrinkle+in+time/lcc_nz_pc/facet_creationdate:%5b1980+TO+1998%5d|facet_rtype:newspaper_articles
    ex: fctN=facet_rtype&fctV=conference_proceedings
    */
    

    
    
    $facets="";
    
    if (count($path_parts)>4){   
    	$max=count($path_parts)-1;
    	for ($x = 4; $x <= $max; $x++) {

    		$facetArray=explode("|",$path_parts[$x]);    	
    		foreach ($facetArray as $f){
    	
    	echo "<p>$f</p>";
    	
    			$q=explode(":", $f);
    			$n=urldecode($q[0]);
    			
    			echo "<p>$n</p>";
    			
    			$fa=$facetMap["$n"];
    			
    			echo "<p>fa: $fa</p>";
    			
    			$v=ltrim($q[1]);
    			
    			if ($fa=="facet_local4"){
    				$y=ltrim(urldecode($v));
    				$v=$libraries["$y"];
    			}
    			
    			if ($fa=="facet_rtype"){
    				$y=ltrim(urldecode($v));
    				$v=$resourceTypes["$y"];
    			}
    			
    			if ($fa=="facet_tlevel"){
    				//$uv=urldecode($v);
  
    				
    				$v=$valueMap["$v"];

    			}
    			
    			
    			if ($fa=="facet_local3"){
    			
    				$v=$v."$"."$"."ILCC";
    			
    			}
    			
    			if ($fa==""){
    			
    				
    			}
    			
    			
    			
    			
    			$facets.="&fctN=$fa&fctV=$v";
    		}
		} 
    }


    /* end, for facets   */
    

	$vid="LCC";
	
	$destination = "http://alliance-primo.hosted.exlibrisgroup.com/primo_library/libweb/action/dlSearch.do?institution=LCC&vid=".$vid."&query=any,contains," . $query . $scope . $source . $facets;
 	//$destination = "http://primo.lclark.edu/primo_library/libweb/action/dlSearch.do?institution=LCC&vid=".$vid."&query=any,contains," . $query . $scope . $source . $facets;

	
	//echo $destination;

    


  	header ('HTTP/1.1 301 Moved Permanently');
  	header ('Location: ' . $destination );

	# TODO: log this in Google Analytics?

  	exit();


?>
