<?php

// splitter page for boley and watzek worldcat instances from primo "search worldcat" button
// dependencies: javascript/primo.js
// 2015

define("TESTING", false);


$incoming=$_SERVER["REQUEST_URI"];
$split=explode("/sharedproxy/?url=", $incoming);
$dbUrl=$split[1];

$queryString=$_GET["queryString"];

$watzekUrl="http://watzek.on.worldcat.org/search?databaseList=&queryString=$queryString";

$boleyUrl="http://lewisclarklaw.on.worldcat.org/search?databaseList=&queryString=$queryString";


$p=explode("?",$incoming);
$params=$p[1];
//echo $params;

echo $_COOKIE["lcsharedproxy"];


//exit();




$setCookies=true;

$userIP=$_SERVER["REMOTE_ADDR"];
$IPbits=explode(".",$userIP);
/*
Watzek IPs:
E 149.175.0.0-149.175.53.255
E 149.175.55.0-149.175.199.255

Boley IPs:
ExcludeIP 149.175.200.1-149.175.255.255
*/

if (TESTING != true){

	if (isset($_COOKIE["lcsharedproxy"])){
		//echo "<p>There's a cookie!</p>";
		$source=$_COOKIE["lcsharedproxy"];
	
		switch($source){
			case "Watzek":
	    			if ($setCookies){header("Location: $watzekUrl");}
				break;
		
			case "Boley":
				if ($setCookies){header("Location: $boleyUrl");}
				break;
		
			case "offCampus":
				//do nothing - present link options below
				break;
		}
	
	} else {

		$source="offCampus";
		if (intval($IPbits[0])==149 && intval($IPbits[1])==175){
			if (intval($IPbits[2])>199){
				$source="Boley";
				if ($setCookies){
					setcookie("lcsharedproxy", $source);
					header("Location: $boleyUrl");
				}		
			} else {
				$source="Watzek";
				if ($setCookies){
					setcookie("lcsharedproxy", $source);
					header("Location: $watzekUrl");
				}
			}

		}
	
	}
	
	
	
}

?>
<!DOCTYPE html>
<html>
<head>
<title>L&C Primo Search : Search WorldCat</title>
<link rel="shortcut icon" href="http://library.lclark.edu/images/favicon.ico" >
<meta charset="utf-8">

<style>
    html{ 
        font-family:verdana, arial, sans-serif;
	font-size:12px;
	background-color:#f5f5f5;
    }
    
    a {
        color:#1c62a8;
        text-decoration:none;
        cursor:pointer;
    }
    
    a:hover {
        color: #3e98df; 
	text-decoration:underline;
    }

    #ezproxybox{
        background-color:#ffffff;
        width:545px;
	height:300px;
        margin:auto;
        padding:25px;
        margin-top:80px;
        -moz-box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2); 
         -webkit-box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2); 
         box-shadow: 1px 1px 5px rgba(0, 0, 0, 0.2); 

    }

	.prompt {
		font-size: 20px;
		color:#888888;
	}

	.boley, .watzek {
		width:90%;
		background-color:#f5f5f5;
		padding:1em;
		margin-top:1em;
		font-size:2em;
		-moz-border-radius: 10px;
		-webkit-border-radius: 10px;
		border-radius: 10px;
		border:1px solid #ddd;
	}



</style> 

<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>

</head>

<body>
	<div id='ezproxybox'>

		<?php

		//var_dump($_COOKIE);

		#sample incoming link: http://library.lclark.edu/sharedproxy?url=http://search.ebscohost.com/login.aspx?authtype=ip,uid&profile=ehost&defaultdb=a9h

		//if (isset($_COOKIE["lcsharedproxy"])){echo "<p>There's a cookie!</p>";}
		//else{echo "<p>there's no cookie!</p>";
		setcookie("lcsharedproxy", $source);

		//}


		
		echo "<p class='prompt'>Click the name of your library, <br />and we'll send you to your library's WorldCat page</p>";

		echo "<div class='boley'><a class='proxylink' title='$boleyUrl' id='Boley'>Paul L. Boley Law Library</a></div>";

		echo "<div class='watzek'><a class='proxylink' title='$watzekUrl' id='Watzek'>Aubrey R. Watzek Library</a></div>";

		?>

		<script>
			$(".proxylink").click(function(){
				var id=$(this).attr("id");
				//alert(id);
				<?php if ($setCookies){echo "document.cookie='lcsharedproxy=' + id;";} ?>
				var redirect=$(this).attr("title");	
				window.location = redirect;

			});


		</script>

  
	</div>

</body>
</html>

